"""
Register New MARMOT gym Environment
"""
from gym.envs.registration import register


register(
    id="MARMOT-v0",
    entry_point="gym_marmot.envs:MARMOTEnv",
)
