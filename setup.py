"""
Setup for the MARMOT OpenAI Gym Environment
"""
from setuptools import setup

setup(name="gym_marmot",
      version="0.1.0",
      description="Multi-agent OpenAI gym environment for Space Applications",
      url="https://gitlab.com/frontierdevelopmentlab/space-resources/gym-marmot",
      license="BSD",
      author="Zahi Kakish",
      author_email="zkakish@gmail.com",
      python_requires='>=3',
      install_requires=["gym", "numpy", "matplotlib", "mapstery", "graphery"]
)